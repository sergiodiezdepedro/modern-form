// Array de preguntas
const preguntas = [
    {
        pregunta: 'Introduzca su nombre de pila'
    },
    {
        pregunta: 'Introduzca sus apellidos'
    },
    {
        pregunta: 'Introduzca su correo',
        pattern: /\S+@\S+\.\S+/
    },
    {
        pregunta: 'Crear su contraseña',
        type: 'password'
    }

];

// Tiempos para transición
const tiempoAgitar = 100;
const tiempoCambioPregunta = 200;

// Posición al inicio en la primera pregunta
let posicion = 0;

// Elementos iniciales del DOM 
const formBox = document.querySelector('#form-box');
const btnSig = document.querySelector('#btn-sig');
const btnPrev = document.querySelector('#btn-prev');
const inputGroup = document.querySelector('#input-group');
const inputField = document.querySelector('#input-field');
const inputLabel = document.querySelector('#input-label');
const inputProgreso = document.querySelector('#input-progreso');
const barraProgreso = document.querySelector('#barra-progreso');

// Eventos
document.addEventListener('DOMContentLoaded', obtenerPregunta);
btnSig.addEventListener('click', validar);

// Input Field activar el enter de teclado
inputField.addEventListener('keyup', e => {
    if (e.keyCode == 13) {
        validar();
    }
});

// Funciones

// Obtener una pregunta del Array y añadirla al marcado del DOM
function obtenerPregunta() {
    inputLabel.innerHTML = preguntas[posicion].pregunta;
    inputField.type = preguntas[posicion].type || 'text';
    inputField.value = preguntas[posicion].respuesta || '';
    inputField.focus();

    // Progreso de la barra, en función del tamaño de la longitud del array
    barraProgreso.style.width = (posicion * 100) / preguntas.length + '%';

    // Añadir icono de usuario o flecha de retroceso
    btnPrev.className = posicion ? 'fas fa-arrow-left' : 'fas fa-user';

    mostrarPregunta();
}

// Mostrar pregunta al usuario 
function mostrarPregunta() {
    inputGroup.style.opacity = 1;
    inputProgreso.style.transition = '';
    inputProgreso.style.width = '100%';
}

// Ocultar la pregunta al usuario
function ocultarPregunta() {
    inputGroup.style.opacity = 0;
    inputLabel.style.marginLeft = 0;
    inputProgreso.style.width = 0;
    inputProgreso.style.transition = 'none';
    inputGroup.style.border = null;
}

// Transformación para crear el movimiento Agitar
function transformar(x, y) {
    formBox.style.transform = `translate(${x}px, ${y}px)`;
}


// Validar el campo
function validar() {
    if (!inputField.value.match(preguntas[posicion].pattern || /.+/)) {
        inputError();
    } else {
        inputExito();
    }
}

// Error en la entrada de datos
function inputError() {
    formBox.className = 'error';
    for (let i = 0; i < 6; i++) {
        setTimeout(transformar, tiempoAgitar * i, ((i % 2) * 2 - 1) * 20, 0);
        setTimeout(transformar, tiempoAgitar * 6, 0, 0);
        inputField.focus();
    }
}

// Éxito en la entrada de datos
function inputExito() {
    formBox.className = '';
    setTimeout(transformar, tiempoAgitar * 0, 0, 10);
    setTimeout(transformar, tiempoAgitar * 1, 0, 0);
    // Guardar las respuestas en el array
    preguntas[posicion].respuesta = inputField.value;

    // Incrementar la posición
    posicion++;

    // Si es una nueva pregunta, ocultar la respuesta actual y obtener la pregunta
    if (preguntas[posicion]) {
        ocultarPregunta();
        obtenerPregunta();
    } else {
        // Ocultar si no hay más preguntas
        ocultarPregunta();
        formBox.className = 'close';
        barraProgreso.style.width = '100%';

        // Formulario completo
        formularioCompleto();
    }
}

// Todos los campos completos - Finalizado el formulario se muestra un mensaje
function formularioCompleto() {
    // console.log(preguntas);
    const h2 = document.createElement('h2');
    h2.classList.add('end');
    h2.appendChild(
        document.createTextNode(
            `Gracias ${
        preguntas[0].respuesta
      }. Ya te has registrado y recibirás un correo pronto.`
        )
    );
    setTimeout(() => {
        formBox.parentElement.appendChild(h2);
        setTimeout(() => (h2.style.opacity = 1), 50);
    }, 1000);
}